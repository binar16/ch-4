class game {
  constructor() {
    // this.cBatu = document.querySelector(".batuc");
    // this.cgunting = document.querySelector(".guntingc");
    // this.cKertas = document.querySelector(".kertasc");
  }

  getPilComp() {
    const comp = Math.random();
    if (comp < 0.34) return "batu";
    if (comp >= 0.34 && comp < 0.67) return "gunting";
    return "kertas";
  }

  getHasil(mycho, comp) {
    //     if (mycho === "batu" && comp === this.cgunting || mycho === "gunting" && comp === this.cKertas || mycho === "kertas" && comp === this.cBatu) return "MENANG"  ;
    // else if (mycho === "gunting" && comp === this.cBatu || mycho === "kertas" && comp === this.cgunting || mycho === "batu" && comp === this.cKertas) return "KALAH";
    // else return "SERI";
    if (mycho == comp) return "DRAW";
    if (mycho == "kertas") return comp == "batu" ? "PLAYER 1 WIN" : "COM WIN";
    if (mycho == "batu") return comp == "gunting" ? "PLAYER 1 WIN" : "COM WIN";
    if (mycho == "gunting") return comp == "kertas" ? "PLAYER 1 WIN" : "COM WIN";
  }
  setBackground(comp) {
    if (comp === "batu") {
      document.querySelector(".batuc").classList.add("pick");
    }
    if (comp === "gunting") {
      document.querySelector(".guntingc").classList.add("pick");
    }
    if (comp === "kertas") {
      document.querySelector(".kertasc").classList.add("pick");
    }
  }
}

run = new game();
let isPlayable = true;

const all = document.querySelectorAll(".play button");
all.forEach(function (all) {
  all.addEventListener(`click`, function () {
    if (isPlayable) {
      // jadi kalau variable ini false, game tidak akan diproses
      // proses lain

      const pilCom = run.getPilComp();
      const senjata = run.setBackground(pilCom);
      const pilPlayer = all.className;
      const result = run.getHasil(pilPlayer, pilCom);

      document.getElementById(
        "vs"
      ).innerHTML = `<h2 class="bg-success">${result}</h2>`;
        all.classList.add("pick");

      console.log("comp : " + pilCom);
      console.log("player : " + pilPlayer);
      console.log("hasil : " + result);
      console.log(pilCom);
      isPlayable = false;
    }
  });
});

const refresh = document.querySelector(".re img");
const semua = document.querySelectorAll(".comuser button")
console.log(semua)
refresh.addEventListener(`click`, function () {
  all.forEach(function (all) {
    all.classList.remove("pick");
  });
  semua.forEach(function(semua){
    semua.classList.remove("pick");
  })
  document.getElementById(
    "vs"
  ).innerHTML = `<h2 class="text-danger">VS</h2>`;
  
  isPlayable = true;
  
});
